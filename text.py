#!/usr/bin/python3
#Program to analyze the sentiment of the text in tweets.

import pandas as pd
import re
import os
from nltk.corpus import stopwords
from collections import Counter

def list_files():
    """Make a list of all the txt files in the directory"""
    f_list=[] 
    for f in os.listdir("."):
        if f.endswith(".txt"):
            f_list.append(f)
    return f_list
 
def lowercasing(f1):
    """Lowercasing all the lines"""
    lines = []
    for line in f1:
        lines.append(line.lower())
    return lines

def location(f3,f4,dic_places):
    """Removing lines that do not contain proper Dutch location"""
    p = set()   
    for line in f3:
        if line not in p:
            p.add(line)
            location,text = line.split("\t")
            for key in dic_places:
                dic2_places = dic_places[key]                
                if location in dic2_places: #check location like Groningen or Maastricht
                    newl = dic2_places[location]
                    f4.write('{0}\t{1}'.format(str(newl),text))
                else:
                    if location.count(",") == 1: #check location like Amsterdam, Nederland or Gelderland, Apeldoorn
                        l1,l2 = location.split(",")
                        if l1 in dic2_places:
                            newl = dic2_places[l1]
                            f4.write('{0}\t{1}'.format(str(newl),text))
                        elif l2 in dic2_places:
                            newl = dic2_places[l2]
                            f4.write('{0}\t{1}'.format(str(newl),text))
                        elif location.count(",") == 2: #location like Amsterdam, Centrum, Nederland or Noord, Nederland, Assen
                            l1,l2,l3 = location.split(",")
                            if l1 in dic2_places:
                                newl = dic2_places[l1]
                                f4.write('{0}\t{1}'.format(str(newl),text))
                            elif l2 in dic2_places:
                                newl = dic2_places[l2]
                                f4.write('{0}\t{1}'.format(str(newl),text))
                            elif l3 in dic2_places:
                                newl = dic2_places[l3]
                                f4.write('{0}\t{1}'.format(str(newl),text))

def punctuation(f5):
    """Removing punctuation and numbers from all the lines"""
    clean_lines = []
    for line in f5:
        clean_lines.append(re.sub("[^a-zA-Z\n\s]", "", line))    
    return clean_lines

def stopwords_lexicon(f7,f8,dic_words,stop_words):
    """Removing stopwords and calculating sentiment score of tweet"""
    for line in f7:
        location,text = line.rstrip().split("\t")
        words = text.split(" ")
        for key in dic_words:
            dic2_words = dic_words[key] 
            ls = []
            for word in words:                
                if word not in stop_words and word in dic2_words: 
                    ls.append(dic2_words[word])
        f8.write('{0}\t{1}\n'.format(location,sum(ls)))  

def dictionary_scores(f9):
    """dictionary with for each province all the sentiment scores of the tweets"""
    dic_scores={}
    for line in f9:
        location,score = line.rstrip().split("\t")
        if location in dic_scores.keys():
            dic_scores[location].append(score)
        else:
            dic_scores[location] = [score]
    return dic_scores

def dictionary_average(dic_scores):
    """dictionary with average sentiment score of all tweets for each province"""
    dic_average_score = {}
    for k in dic_scores:
        dic_scores[k] = list(map(float,dic_scores[k]))
        dic_average_score[k] = (int(sum(dic_scores[k])))/len(dic_scores[k])
    return dic_average_score
    
def concatenate_dics(list_dic_average_score):
    """Concatenate all the dictionaries to one dictionary"""
    c=Counter()
    for dic in list_dic_average_score:
        c.update(dic)
    return c

def main():
    list_dic_average_score = []
    f_list = list_files()            
    for f in f_list:            
        with open(f, 'r') as f1, open('lowercase.txt', 'w') as f2:   
            lines = lowercasing(f1)
            f2.writelines(lines)
            
        with open("lowercase.txt","r") as f3, open("location.txt","w") as f4:
            #dic_places = pd.read_excel('municipalities.xlsx', index_col=0).to_dict() #dictionary with place as key and the municipality as value
            dic_places = pd.read_excel('provinces.xlsx', index_col=0).to_dict() #dictionary with place as key and the province as value
            location(f3,f4,dic_places)  

        with open('location.txt', 'r') as f5, open("punctuation.txt","w") as f6:
            clean_lines = punctuation(f5)
            f6.writelines(clean_lines)

        with open("punctuation.txt","r") as f7, open("stopwords.txt","w") as f8:
            dic_words = pd.read_excel('lexicon.xlsx', index_col=0).to_dict() #dictionary with word as key and sentiment score as value
            stop_words = stopwords.words('dutch') #dutch stopwords
            stopwords_lexicon(f7,f8,dic_words,stop_words)     
            
        with open("stopwords.txt","r") as f9: 
            dic_scores = dictionary_scores(f9)
            dic_average_score = dictionary_average(dic_scores)
            list_dic_average_score.append(dic_average_score.copy())

    sum_dic = concatenate_dics(list_dic_average_score)
    print(dict(sum_dic))

if __name__ == '__main__':
	main()

