#!/usr/bin/python3
#Program to convert the dictionaries for each month to a dictionary for the year.

import csv
from collections import Counter
	
def concatenate_dics(dictionaries):
    """Concatenate all the dictionary to one dictionary"""
    c=Counter()
    for dic in dictionaries:
        c.update(dic)  
    return c    

def dic_to_file(results,c):
    """Convert dictionary to .csv file"""
    for k, v in c.items():
        results.writerow([k, v])
    return results  
    	
def main():		 
    list_dic_text = []
    dic_january_text = {'noordholland': 87.56181329464553, 'limburg': 86.4291493571118, 'friesland': 86.71059062502827, 'gelderland': 99.19438904728098, 'drenthe': 76.5511826809515, 'noordbrabant': 96.49306257155364, 'flevoland': 98.78841325721224, 'groningen': 95.05794176277807, 'utrecht': 103.37860987355589, 'zeeland': 105.88132078626954, 'overijssel': 82.97723659067816, 'zuidholland': 101.10816061811624}
    list_dic_text.append(dic_january_text.copy()) 
    dic_february_text = {'limburg': 75.53782525274755, 'zuidholland': 83.96084605805927, 'noordbrabant': 79.97440093956341, 'gelderland': 85.64889770889066, 'groningen': 81.47562492715372, 'flevoland': 82.5928160334236, 'zeeland': 91.61741866203921, 'overijssel': 69.72842728819809, 'friesland': 71.22417245385444, 'utrecht': 87.84826506404376, 'noordholland': 77.23733682769563, 'drenthe': 66.57390157080027}
    list_dic_text.append(dic_february_text.copy()) 
    dic_march_text = {'gelderland': 105.45960095650013, 'friesland': 82.2929561568105, 'groningen': 88.15750396196393, 'overijssel': 79.87892401481479, 'utrecht': 99.1792035383106, 'drenthe': 86.79501833873906, 'limburg': 81.92832825778129, 'flevoland': 84.69602675054948, 'noordbrabant': 91.20708194210097, 'zuidholland': 105.1284063566393, 'noordholland': 85.67499758815396, 'zeeland': 100.74453119229489}
    list_dic_text.append(dic_march_text.copy()) 
    dic_april_text = {'noordbrabant': 84.95330637995019, 'zeeland': 88.77231293457498, 'overijssel': 75.16603034329142, 'zuidholland': 90.13458240649605, 'limburg': 80.60894853700329, 'flevoland': 77.51874278832008, 'groningen': 87.94537540501477, 'friesland': 81.80644016823818, 'noordholland': 79.9582291112429, 'drenthe': 93.78462304827418, 'utrecht': 96.55678971709088, 'gelderland': 90.5380465807048}
    list_dic_text.append(dic_april_text.copy()) 
    dic_may_text = {'friesland': 88.10958282543085, 'drenthe': 97.66297776559321, 'groningen': 86.84730519359765, 'zuidholland': 93.86035252237865, 'gelderland': 101.96756130482302, 'zeeland': 86.0786158499757, 'noordbrabant': 92.07400396823395, 'utrecht': 101.8129259114951, 'noordholland': 83.83067147285898, 'limburg': 86.6685782215901, 'overijssel': 77.49751954840488, 'flevoland': 78.7283889599444}
    list_dic_text.append(dic_may_text.copy()) 
    dic_june_text = {'overijssel': 75.83876368800051, 'limburg': 81.48201796129467, 'noordbrabant': 90.71095206644608, 'noordholland': 82.51251881363376, 'flevoland': 84.50122612045041, 'friesland': 90.25396466382844, 'drenthe': 95.16383082801575, 'gelderland': 107.1597597439447, 'utrecht': 100.58491664970123, 'groningen': 86.9797125418798, 'zuidholland': 91.3003211956835, 'zeeland': 88.04299899976841}
    list_dic_text.append(dic_june_text.copy()) 
    dic_july_text = {'zeeland': 89.28703335554985, 'noordholland': 84.04283414509992, 'flevoland': 90.22011099755007, 'limburg': 81.54701687219072, 'drenthe': 94.40819282289121, 'zuidholland': 96.42838534402114, 'utrecht': 99.23524779066048, 'noordbrabant': 90.96264999155603, 'overijssel': 76.99460319840448, 'groningen': 91.61625973376074, 'friesland': 90.37405947370547, 'gelderland': 107.56742979671546}
    list_dic_text.append(dic_july_text.copy()) 
    dic_august_text = {'friesland': 83.9107898415682, 'gelderland': 92.77355350376295, 'zuidholland': 90.79986712833704, 'noordbrabant': 86.67482524901473, 'limburg': 82.98031379417266, 'overijssel': 74.76030282128256, 'groningen': 86.3642997613663, 'utrecht': 95.57406551220558, 'noordholland': 81.73632565175166, 'flevoland': 88.01016391646773, 'zeeland': 78.83576071791539, 'drenthe': 89.10627420263239}
    list_dic_text.append(dic_august_text.copy()) 
    dic_september_text = {'groningen': 79.89912501569374, 'zeeland': 74.52972654962402, 'friesland': 87.54015778886618, 'limburg': 78.25814641563863, 'noordbrabant': 82.74889774942318, 'flevoland': 82.84420143683585, 'overijssel': 67.69937586934284, 'noordholland': 78.40477100963079, 'zuidholland': 87.14463537363294, 'gelderland': 88.53587910770464, 'utrecht': 90.87918111015817, 'drenthe': 88.11831690714708}
    list_dic_text.append(dic_september_text.copy()) 
    dic_october_text = {'drenthe': 89.0686781678164, 'groningen': 80.39994659239855, 'zuidholland': 86.67806728572413, 'gelderland': 90.31075490493156, 'limburg': 80.0465076884009, 'utrecht': 92.69299010965547, 'zeeland': 82.55358412722832, 'noordholland': 81.14018037359865, 'overijssel': 68.3071556364186, 'flevoland': 81.03106796375097, 'friesland': 84.74181740552567, 'noordbrabant': 86.0820820833803}
    list_dic_text.append(dic_october_text.copy()) 
    dic_november_text = {'noordholland': 79.91807789122426, 'flevoland': 82.14439475714224, 'zuidholland': 88.01617617173483, 'zeeland': 84.55115926199588, 'friesland': 82.71475709302844, 'gelderland': 88.24168267817922, 'noordbrabant': 85.04406500813536, 'overijssel': 66.71349484303394, 'limburg': 73.786561152685, 'drenthe': 86.97637724951, 'utrecht': 91.77460201606043, 'groningen': 81.02194211891916}
    list_dic_text.append(dic_november_text.copy()) 
    dic_december_text = {'noordbrabant': 95.8908037619749, 'flevoland': 95.79547842219611, 'zuidholland': 96.54905565294469, 'drenthe': 95.2773095215529, 'friesland': 91.84327835436697, 'noordholland': 85.24191921610115, 'gelderland': 97.6152532216305, 'groningen': 91.65574615103341, 'limburg': 88.33700879094589, 'zeeland': 93.18938267371557, 'utrecht': 105.63908228320659, 'overijssel': 72.83762990871863}
    list_dic_text.append(dic_december_text.copy()) 
    
    dictionaries = list_dic_text 
    c = concatenate_dics(dictionaries) 
    results = csv.writer(open("results_text_2017_provinces.csv", "w"))    
    r = dic_to_file(results,c)  
         
main()            
