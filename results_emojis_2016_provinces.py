#!/usr/bin/python3
#Program to convert the dictionaries for each month to a dictionary for the year.

import csv
from collections import Counter
	
def concatenate_dics(dictionaries):
    """Concatenate all the dictionary to one dictionary"""
    c=Counter()
    for dic in dictionaries:
        c.update(dic)  
    return c    	

def dic_to_file(results,c):
    """Convert dictionary to .csv file"""
    for k, v in c.items():
        results.writerow([k, v])
    return results        
   
def main():		
    list_dic_emoji = []
    dic_january_emoji = {'limburg': 322.33809523809515, 'noord-holland': 333.09929181929175, 'utrecht': 388.868668749551, 'overijssel': 546.5273018648018, 'groningen': 247.4607142857143, 'flevoland': 185.61666666666667, 'friesland': 308.2809523809524, 'zuid-holland': 497.3706091888861, 'gelderland': 357.62857142857126, 'noord-brabant': 447.71528194028207, 'zeeland': 184.61038961038966, 'drenthe': 110.8409090909091}
    list_dic_emoji.append(dic_january_emoji.copy())
    dic_february_emoji = {'flevoland': 214.3083333333334, 'friesland': 250.31666666666666, 'noord-holland': 332.9889249639249, 'drenthe': 129.99324324324326, 'groningen': 238.615359879584, 'gelderland': 229.6095238095238, 'limburg': 313.2833333333333, 'overijssel': 463.98579417658374, 'zuid-holland': 473.1809197838846, 'noord-brabant': 444.2880341880343, 'utrecht': 371.6448384948383, 'zeeland': 165.74722222222223}
    list_dic_emoji.append(dic_february_emoji.copy())
    dic_march_emoji = {'overijssel': 464.8400724275725, 'gelderland': 264.2214285714285, 'drenthe': 83.83333333333334, 'zeeland': 221.32134254634258, 'friesland': 251.48333333333332, 'zuid-holland': 475.56750181075546, 'noord-holland': 353.91703880914406, 'utrecht': 372.9896825396824, 'limburg': 251.25476190476195, 'groningen': 204.16849816849813, 'noord-brabant': 359.48039796460824, 'flevoland': 177.19841269841274}
    list_dic_emoji.append(dic_march_emoji.copy())    
    dic_april_emoji = {'groningen': 196.93253968253967, 'flevoland': 203.16724386724385, 'drenthe': 125.95, 'gelderland': 304.5666666666665, 'noord-brabant': 351.39404761904757, 'zuid-holland': 486.3759715847439, 'overijssel': 472.34966422466414, 'limburg': 218.5047619047619, 'friesland': 320.29523809523806, 'noord-holland': 396.641847041847, 'utrecht': 371.28571428571433, 'zeeland': 219.6161616161616}
    list_dic_emoji.append(dic_april_emoji.copy())   
    dic_may_emoji = {'friesland': 255.4083333333333, 'noord-brabant': 360.39989177489156, 'zeeland': 233.73650793650793, 'groningen': 269.09722222222223, 'gelderland': 360.37173382173387, 'utrecht': 393.7333333333334, 'zuid-holland': 472.4002291655753, 'flevoland': 226.63035159285164, 'noord-holland': 370.6588012763471, 'overijssel': 496.35169332583126, 'limburg': 196.69444444444446, 'drenthe': 117.04761904761902}
    list_dic_emoji.append(dic_may_emoji.copy())   
    dic_june_emoji = {'friesland': 262.1, 'gelderland': 281.34761904761916, 'groningen': 355.472062732589, 'zuid-holland': 492.46537716119917, 'drenthe': 109.86666666666666, 'noord-brabant': 362.19047619047626, 'limburg': 211.11666666666665, 'overijssel': 469.90334314807995, 'flevoland': 161.40844155844158, 'zeeland': 132.50714285714284, 'utrecht': 397.47579365079366, 'noord-holland': 360.0504997926051}
    list_dic_emoji.append(dic_june_emoji.copy())   
    dic_july_emoji = {'noord-brabant': 365.79329004329, 'zeeland': 143.24166666666665, 'drenthe': 113.08333333333333, 'utrecht': 370.08993506493505, 'groningen': 334.2059523809523, 'zuid-holland': 495.7666252830427, 'overijssel': 504.3882850809317, 'friesland': 272.03809523809525, 'gelderland': 307.90692640692635, 'flevoland': 239.9724386724387, 'noord-holland': 381.3016638916637, 'limburg': 187.16666666666663}
    list_dic_emoji.append(dic_july_emoji.copy())   
    dic_august_emoji = {'noord-brabant': 272.20952380952383, 'noord-holland': 328.26125541125555, 'utrecht': 399.59570707070696, 'zeeland': 138.075, 'gelderland': 348.09722222222234, 'flevoland': 169.13852813852813, 'groningen': 277.77092352092353, 'overijssel': 523.6247135217724, 'friesland': 240.41428571428577, 'drenthe': 118.9621212121212, 'limburg': 135.75, 'zuid-holland': 537.2048237202935}
    list_dic_emoji.append(dic_august_emoji.copy())   
    dic_september_emoji = {'groningen': 245.11190476190473, 'noord-brabant': 246.10000000000005, 'noord-holland': 329.51487678987684, 'overijssel': 509.13630519806986, 'zeeland': 117.89285714285714, 'zuid-holland': 469.8376708788954, 'gelderland': 283.10238095238094, 'friesland': 237.68484848484854, 'drenthe': 89.78333333333332, 'utrecht': 331.09087301587306, 'limburg': 134.0, 'flevoland': 168.35191752691753}
    list_dic_emoji.append(dic_september_emoji.copy())   
    dic_october_emoji = {'zeeland': 149.03923006817743, 'zuid-holland': 457.85790932591647, 'limburg': 171.81666666666666, 'drenthe': 97.24999999999999, 'utrecht': 323.3900793650794, 'overijssel': 507.5571872571871, 'friesland': 237.8380952380953, 'noord-holland': 403.38693130540946, 'flevoland': 218.8266233766234, 'groningen': 228.21666666666673, 'noord-brabant': 281.6190476190477, 'gelderland': 337.8261904761905}
    list_dic_emoji.append(dic_october_emoji.copy())   
    dic_november_emoji = {'noord-holland': 362.20743145743137, 'friesland': 194.7238095238095, 'zeeland': 104.41666666666667, 'overijssel': 457.3105269730268, 'drenthe': 99.23809523809523, 'utrecht': 360.7768009768009, 'noord-brabant': 325.1003968253968, 'zuid-holland': 450.75852419167296, 'limburg': 161.88888888888889, 'groningen': 191.89999999999998, 'flevoland': 187.7809523809524, 'gelderland': 334.9278998778998} 
    list_dic_emoji.append(dic_november_emoji.copy())   
    dic_december_emoji = {'flevoland': 119.31930014430013, 'overijssel': 494.1174346851978, 'noord-brabant': 266.9007936507936, 'zuid-holland': 486.9246388757983, 'utrecht': 391.96327561327564, 'gelderland': 406.33744588744565, 'limburg': 193.13441558441554, 'drenthe': 96.91666666666667, 'zeeland': 93.0, 'groningen': 245.4333333333333, 'friesland': 213.9, 'noord-holland': 424.25952380952367}
    list_dic_emoji.append(dic_december_emoji.copy())   
    
    dictionaries = list_dic_emoji  
    c = concatenate_dics(dictionaries)                     
    results = csv.writer(open("results_emoji_2016_provinces.csv", "w"))
    r = dic_to_file(results,c)  
                  
main()      
