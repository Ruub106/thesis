This repository contains all the files and programs that I used for my bachelor thesis. In each of the programs an explanation of what the program exactly does is stated on the second line of the program. The data that is being used for this study comes from the twitter2 corpus of the Faculty of Arts of the University of Groningen.

