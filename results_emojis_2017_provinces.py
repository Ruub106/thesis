#!/usr/bin/python3
#Program to convert the dictionaries for each month to a dictionary for the year.

import csv
from collections import Counter
	
def concatenate_dics(dictionaries):
    """Concatenate all the dictionary to one dictionary"""
    c=Counter()
    for dic in dictionaries:
        c.update(dic)  
    return c    	

def dic_to_file(results,c):
    """Convert dictionary to .csv file"""
    for k, v in c.items():
        results.writerow([k, v])
    return results        
   
def main():		
    list_dic_emoji = []
    dic_january_emoji = {'noord-holland': 406.50602453102465, 'drenthe': 104.5, 'groningen': 200.78333333333333, 'overijssel': 444.2851148851149, 'friesland': 221.87380952380954, 'noord-brabant': 327.5468253968254, 'utrecht': 350.5706349206348, 'zuid-holland': 485.57238696890863, 'gelderland': 371.0992063492064, 'flevoland': 172.3987373737374, 'limburg': 198.78333333333333, 'zeeland': 148.61666666666667}
    list_dic_emoji.append(dic_january_emoji.copy())
    dic_february_emoji = {'utrecht': 398.5598956598955, 'noord-holland': 434.7873626373627, 'friesland': 248.91904761904783, 'overijssel': 464.6431443556444, 'zeeland': 146.73333333333335, 'noord-brabant': 305.3583333333332, 'groningen': 150.08333333333331, 'zuid-holland': 421.26232720577804, 'gelderland': 380.39635642135624, 'limburg': 165.480303030303, 'flevoland': 161.03333333333333, 'drenthe': 89.5}
    list_dic_emoji.append(dic_february_emoji.copy())
    dic_march_emoji =  {'flevoland': 171.97261904761905, 'drenthe': 99.83333333333333, 'utrecht': 402.2901536372124, 'noord-brabant': 332.80915750915756, 'zuid-holland': 510.7981048581047, 'limburg': 189.1833333333333, 'noord-holland': 454.63920523920484, 'zeeland': 187.2738095238095, 'groningen': 245.00000000000009, 'friesland': 282.7214285714286, 'overijssel': 517.2799006549003, 'gelderland': 410.1715895215893}
    list_dic_emoji.append(dic_march_emoji.copy())    
    dic_april_emoji = {'gelderland': 315.9242063492064, 'friesland': 315.2071428571429, 'flevoland': 209.75804473304473, 'utrecht': 378.7351606726607, 'zeeland': 155.81666666666666, 'noord-brabant': 258.47916666666663, 'limburg': 171.33333333333334, 'overijssel': 524.9451102818747, 'groningen': 187.3, 'drenthe': 108.73333333333335, 'noord-holland': 327.2646825396824, 'zuid-holland': 403.8394805194804}
    list_dic_emoji.append(dic_april_emoji.copy())   
    dic_may_emoji = {'friesland': 348.89877344877345, 'gelderland': 305.8718253968253, 'zeeland': 186.81190476190474, 'overijssel': 499.60612426788913, 'groningen': 240.96428571428572, 'limburg': 167.27196969696968, 'drenthe': 99.83333333333334, 'flevoland': 148.60234487734485, 'noord-holland': 379.65277910495314, 'zuid-holland': 505.2372613296523, 'utrecht': 420.2588023088024, 'noord-brabant': 267.37261904761914}
    list_dic_emoji.append(dic_may_emoji.copy())   
    dic_june_emoji = {'groningen': 241.95000000000002, 'gelderland': 327.0102813852814, 'zeeland': 252.24646464646474, 'zuid-holland': 475.0276341050253, 'overijssel': 505.59954212454204, 'noord-brabant': 281.46666666666664, 'flevoland': 148.08333333333331, 'drenthe': 70.66666666666666, 'utrecht': 401.576443001443, 'limburg': 181.74999999999997, 'noord-holland': 335.75, 'friesland': 291.73434065934066}
    list_dic_emoji.append(dic_june_emoji.copy())   
    dic_july_emoji = {'zuid-holland': 522.1522444946359, 'utrecht': 435.6581529581527, 'overijssel': 478.14003774003754, 'groningen': 236.80833333333334, 'flevoland': 169.69523809523807, 'zeeland': 267.5365079365079, 'noord-holland': 382.5768620268619, 'gelderland': 308.8150793650792, 'friesland': 258.2452380952381, 'limburg': 166.25000000000003, 'drenthe': 98.86666666666666, 'noord-brabant': 268.8833333333333}
    list_dic_emoji.append(dic_july_emoji.copy())   
    dic_august_emoji = {'gelderland': 331.6809523809523, 'flevoland': 153.3, 'overijssel': 416.08531746031747, 'zuid-holland': 488.34343814080654, 'noord-holland': 409.4694699091757, 'noord-brabant': 297.37460317460324, 'friesland': 217.4968253968254, 'drenthe': 105.66666666666666, 'utrecht': 459.2233044733046, 'limburg': 172.91666666666666, 'groningen': 268.80238095238093, 'zeeland': 258.86071428571427}
    list_dic_emoji.append(dic_august_emoji.copy())   
    dic_september_emoji = {'overijssel': 380.5069805194806, 'noord-brabant': 278.72142857142865, 'gelderland': 300.98095238095243, 'zuid-holland': 461.46636321195155, 'friesland': 206.76190476190473, 'utrecht': 421.9330697080694, 'groningen': 248.21428571428572, 'flevoland': 164.69761904761907, 'zeeland': 320.06349206349205, 'limburg': 132.5, 'noord-holland': 390.43921070404457, 'drenthe': 83.0}
    list_dic_emoji.append(dic_september_emoji.copy())   
    dic_october_emoji = {'groningen': 257.3690476190476, 'noord-brabant': 223.06428571428577, 'friesland': 223.60714285714286, 'noord-holland': 338.9582070707071, 'zeeland': 274.00659063159065, 'overijssel': 445.4079281829282, 'zuid-holland': 465.1219846229284, 'utrecht': 416.1348924467343, 'flevoland': 169.3425685425685, 'gelderland': 382.13888888888897, 'limburg': 97.03333333333335, 'drenthe': 103.33333333333333}
    list_dic_emoji.append(dic_october_emoji.copy())   
    dic_november_emoji = {'friesland': 183.8928571428571, 'noord-brabant': 272.37619047619046, 'utrecht': 412.8022457934221, 'overijssel': 480.7582556332556, 'zuid-holland': 492.2491053837802, 'drenthe': 50.25, 'limburg': 128.25, 'noord-holland': 372.99166666666673, 'gelderland': 412.44834054834035, 'groningen': 252.65238095238095, 'flevoland': 195.07175324675316, 'zeeland': 293.4972222222222}
    list_dic_emoji.append(dic_november_emoji.copy())   
    dic_december_emoji = {'utrecht': 427.2397395088571, 'limburg': 100.5, 'zuid-holland': 423.6181327444485, 'flevoland': 145.49166666666667, 'overijssel': 458.1526473526472, 'noord-brabant': 263.8166666666667, 'noord-holland': 358.79880952380944, 'gelderland': 478.560248085248, 'groningen': 348.9666666666667, 'drenthe': 85.66666666666667, 'zeeland': 273.7384920634921, 'friesland': 193.9416666666667}
    list_dic_emoji.append(dic_december_emoji.copy())   
    
    dictionaries = list_dic_emoji  
    c = concatenate_dics(dictionaries)                     
    results = csv.writer(open("results_emoji_2017_provinces.csv", "w"))
    r = dic_to_file(results,c)  
                  
main()            
        
        
