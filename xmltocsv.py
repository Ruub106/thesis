#!/usr/bin/python3
#Program to convert .xml to .csv file, filtered on "word" and "form".

import xml.etree.ElementTree as ET
import csv
                    
def main():
    tree = ET.parse("nl-sentiment.xml")
    root = tree.getroot()
    csv_file = open("lexicon.csv","w")
    csvwriter = csv.writer(csv_file)
    for member in root.findall("word"):
        sentiment = []
        form = member.find("form").text
        sentiment.append(form)
        polarity = member.find("polarity").text
        sentiment.append(polarity) 
        csvwriter.writerow(sentiment)     
    csv_file.close()     
       		
if __name__ == '__main__':
	main()
