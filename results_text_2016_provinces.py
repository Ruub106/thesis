#!/usr/bin/python3
#Program to convert the dictionaries for each month to a dictionary for the year.

import csv
from collections import Counter
	
def concatenate_dics(dictionaries):
    """Concatenate all the dictionary to one dictionary"""
    c=Counter()
    for dic in dictionaries:
        c.update(dic)  
    return c    

def dic_to_file(results,c):
    """Convert dictionary to .csv file"""
    for k, v in c.items():
        results.writerow([k, v])
    return results  
    	
def main():		 
    list_dic_text = []
    dic_january_text = {'zeeland': 114.0937163375559, 'utrecht': 103.57187012542317, 'noordholland': 83.6415075400758, 'drenthe': 86.5006892024006, 'gelderland': 101.2569959899038, 'groningen': 90.17432499321109, 'overijssel': 80.41620518481369, 'zuidholland': 95.75022276336517, 'noordbrabant': 96.9356057250621, 'limburg': 92.61079247637267, 'friesland': 86.99155946882613, 'flevoland': 91.14532926895315}
    list_dic_text.append(dic_january_text.copy()) 
    dic_february_text = {'overijssel': 70.96531072631254, 'friesland': 79.85833539656306, 'zuidholland': 92.15422898763318, 'gelderland': 89.08630351272039, 'utrecht': 94.50895441267475, 'flevoland': 90.25180293900536, 'limburg': 83.1541083323314, 'noordholland': 77.61409880275099, 'groningen': 86.29599697999898, 'zeeland': 102.51562996975454, 'noordbrabant': 85.47739781194183, 'drenthe': 78.12442368124525}
    list_dic_text.append(dic_february_text.copy()) 
    dic_march_text = {'noordbrabant': 83.36527129804084, 'noordholland': 74.67586667269867, 'overijssel': 68.9489307393374, 'limburg': 78.09813557930978, 'gelderland': 89.65662328388342, 'flevoland': 85.34223800217643, 'zeeland': 104.34789302075636, 'zuidholland': 93.69857971618598, 'drenthe': 81.18917225001346, 'groningen': 84.60266460294885, 'utrecht': 92.99445523812432, 'friesland': 78.24133757990361}
    list_dic_text.append(dic_march_text.copy()) 
    dic_april_text = {'gelderland': 94.52817203955456, 'overijssel': 73.88093038792901, 'utrecht': 96.79485891117965, 'flevoland': 94.06181593723588, 'noordholland': 80.3805800644099, 'friesland': 81.7335718985669, 'limburg': 81.8965764394684, 'drenthe': 91.20495963079998, 'zeeland': 109.91484356378, 'zuidholland': 95.45782924397395, 'noordbrabant': 92.15419392645349, 'groningen': 91.92109766343805}
    list_dic_text.append(dic_april_text.copy()) 
    dic_may_text = {'drenthe': 90.80484644157964, 'utrecht': 101.46119126152993, 'limburg': 90.96366130579041, 'groningen': 96.84401669862517, 'noordholland': 83.76039129575116, 'gelderland': 102.76559652860996, 'flevoland': 100.79177407995546, 'overijssel': 78.12560182696501, 'noordbrabant': 104.49974335606713, 'friesland': 87.80783765328984, 'zuidholland': 99.20392901665336, 'zeeland': 110.33503092617539}
    list_dic_text.append(dic_may_text.copy()) 
    dic_june_text = {'drenthe': 91.37394301007473, 'overijssel': 74.35781791062466, 'limburg': 85.20400058287439, 'noordholland': 80.32038398936254, 'friesland': 92.4047159370881, 'utrecht': 96.91692998772498, 'zuidholland': 95.2010543378966, 'flevoland': 94.90805580632127, 'gelderland': 98.92333586605204, 'noordbrabant': 97.79959215648054, 'groningen': 92.94962515329243, 'zeeland': 98.79084482117497}
    list_dic_text.append(dic_june_text.copy()) 
    dic_july_text = {'friesland': 94.4304226066376, 'flevoland': 100.98922193892483, 'gelderland': 97.79162577565722, 'overijssel': 73.20754792158635, 'utrecht': 102.86704294810464, 'groningen': 96.71917775581309, 'zeeland': 99.25973867007173, 'limburg': 87.11430885777048, 'noordbrabant': 99.86281423790058, 'drenthe': 86.13585546604732, 'noordholland': 84.48147320249842, 'zuidholland': 96.8944719371503}
    list_dic_text.append(dic_july_text.copy()) 
    dic_august_text = {'utrecht': 103.80015964763456, 'noordbrabant': 100.59147210934661, 'flevoland': 99.64911579003525, 'gelderland': 98.64987354594426, 'overijssel': 75.09847108277711, 'zeeland': 98.03252214956159, 'groningen': 95.34427891408663, 'drenthe': 95.96703164431237, 'limburg': 90.28055538403314, 'friesland': 98.72269307798702, 'zuidholland': 98.75205750990403, 'noordholland': 85.94409764684019}
    list_dic_text.append(dic_august_text.copy()) 
    dic_september_text = {'noordholland': 82.8524792904666, 'drenthe': 78.3595667289091, 'gelderland': 97.20473961462692, 'zeeland': 102.58705352330226, 'friesland': 92.04267325467524, 'limburg': 82.13904829785047, 'zuidholland': 92.64642781486255, 'utrecht': 100.87258544191513, 'flevoland': 96.8463456143557, 'noordbrabant': 93.338721111432, 'groningen': 91.94491734229616, 'overijssel': 72.71076164500445}
    list_dic_text.append(dic_september_text.copy()) 
    dic_october_text = {'flevoland': 101.87048446669976, 'noordholland': 85.43804698806203, 'limburg': 84.21710463841073, 'utrecht': 101.7002767260733, 'zuidholland': 95.61924069469895, 'overijssel': 74.48971177314374, 'gelderland': 98.4690832698045, 'noordbrabant': 94.22895752502579, 'drenthe': 78.06978740704618, 'zeeland': 103.95344064453252, 'groningen': 91.49962399792419, 'friesland': 90.30622299018654}
    list_dic_text.append(dic_october_text.copy()) 
    dic_november_text = {'noordholland': 83.1804515793336, 'noordbrabant': 89.17210782797123, 'limburg': 79.94523055497991, 'drenthe': 77.40953929444981, 'zeeland': 100.5001575600077, 'utrecht': 98.3161370435574, 'friesland': 81.85502133138738, 'gelderland': 92.8509652888416, 'overijssel': 74.44773512386195, 'zuidholland': 91.12347411082294, 'flevoland': 96.55441032203414, 'groningen': 92.36214117165694}
    list_dic_text.append(dic_november_text.copy()) 
    dic_december_text = {'friesland': 93.89298348262122, 'drenthe': 89.80773948031238, 'noordholland': 92.25700894732505, 'utrecht': 108.33154083298307, 'zeeland': 111.39131531748498, 'noordbrabant': 103.44016411129702, 'flevoland': 104.03437117650554, 'gelderland': 101.77682925442862, 'limburg': 94.09216808188873, 'zuidholland': 106.02209035381105, 'overijssel': 84.98144621245471, 'groningen': 101.71560046294688}
    list_dic_text.append(dic_december_text.copy()) 
    
    dictionaries = list_dic_text 
    c = concatenate_dics(dictionaries) 
    results = csv.writer(open("results_text_2016_provinces.csv", "w"))    
    r = dic_to_file(results,c)  
         
main()            
